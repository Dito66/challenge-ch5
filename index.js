const express = require("express");
const app = express();
const port = 3000;

app.listen(8000);
app.set("view engine", "ejs");
app.use("/", express.static("public"));
app.use(express.urlencoded({ extended: true }));

app.get("/login", (req, res) => {
  let error = {
    message: req.query.error || "Please login",
  };
  res.render("login", error);
});

app.post("/login", (req, res) => {
  // logic.....
  try {
    let { email, password } = req.body;

    if (email == "sabrina@mail.com" && password == "123456") {
      res.redirect("/game.html");
      return;
    }
  } catch (e) {
    console.log(e.message);
  }

  res.redirect("/login?error=invalidid%20username");
});

app.listen(port, () => console.log(`server running on port ${port}`));
