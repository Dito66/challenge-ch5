//Variabel
const b_user = document.getElementById("batu");
const k_user = document.getElementById("kertas");
const g_user = document.getElementById("gunting");
const b_com = document.getElementById("cbatu");
const k_com = document.getElementById("ckertas");
const g_com = document.getElementById("cgunting");
const winBox = document.getElementById("box");
const inFo = document.getElementById("h1");
const refresh = document.getElementById("refresh");
const x = document.querySelector(".u-tool");
const addElement1 = [...document.getElementsByClassName("ganti")];
const button = document.querySelector("button");

//Pilihan Com
function comThink() {
  var choices = ["Batu", "Gunting", "Kertas"];
  var randomChoices = Math.floor(Math.random() * 3);
  return choices[randomChoices];
}
//Kotak tengah
function resultObject() {
  winBox.classList.add("winBox"), inFo.setAttribute("style", "font-size:20px; margin-top:-53px; margin-left:0px; color:white;");
}
function resultDraw() {
  winBox.classList.add("drawBox");

  inFo.setAttribute("style", "font-size:20px; margin-top:-53px; margin-left:0px; color:white;");
}

//Text kotak tengah
function win() {
  console.log("Player 1 Win");
  resultObject();
  inFo.innerText = "Player 1 WIN";
}

function lose() {
  console.log("COM WIN");
  resultObject();

  inFo.innerText = "COM WIN";
}

function draw() {
  console.log("Draw");
  resultDraw();

  inFo.innerText = "Draw";
}

// rules
function gameCompare(pilihanUser) {
  const computerUser = comThink();
  console.log("Hasil User => " + pilihanUser);
  console.log("Hasil dari => " + computerUser);

  switch (pilihanUser + computerUser) {
    case "BatuGunting":
    case "GuntingKertas":
    case "KertasBatu":
      win();

      break;
    case "GuntingBatu":
    case "BatuKertas":
    case "KertasGunting":
      lose();
      break;
    case "GuntingGunting":
    case "BatuBatu":
    case "KertasKertas":
      draw();
  }

  switch (computerUser) {
    case "Batu":
      b_com.classList.add("chosen");

      break;
    case "Gunting":
      g_com.classList.add("chosen");
      break;
    case "Kertas":
      k_com.classList.add("chosen");
  }
}

//Pilihan player
function play() {
  b_user.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Batu");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });

  k_user.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Kertas");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });

  g_user.addEventListener("click", function () {
    this.classList.add("chosen");
    gameCompare("Gunting");
    addElement1.forEach((addElement3) => {
      addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;");
    });
  });
}

play();

// Refresh
refresh.addEventListener("click", function () {
  addElement1.forEach((addElement2) => {
    addElement2.classList.remove("chosen");
  });
  addElement1.forEach((addElement3) => {
    addElement3.removeAttribute("style", "cursor: not-allowed;pointer-events: none;");
  });
  winBox.classList.remove("winBox");
  winBox.classList.remove("drawBox");
  inFo.removeAttribute("style", "color: ''; font-size:'' ");

  inFo.style.marginTop = null;
  inFo.style.fontSize = null;
  inFo.innerText = "VS";
  button.disabled = false;
});
